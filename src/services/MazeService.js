class Maze {

    payload = {};
    success = false;

    static randomNumber (min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static create (
        map, brickColumnCount, brickRowsCount, brickW, brickH, xStart, yStart, xEnd, yEnd, codeEmptyBrick,
        codeFillBrick, codeStartBrick, codeFinishBrick, mazeDifficulty
    ) {

        for (let column = 0; column < brickColumnCount; column++) {
            map[column] = [];
            for (let row = 0; row < brickRowsCount; row++) {
                let random = this.randomNumber(0, mazeDifficulty + 2);
                map[column][row] = {
                    x: column * (brickW + 3),
                    y: row * (brickH + 3),
                    state: random > 0 ? codeEmptyBrick : codeFillBrick
                }
            }
        }

        map[xStart][yStart].state = codeStartBrick;
        map[xEnd][yEnd].state = codeFinishBrick;

        return map;
    }

    static solve (
        map, brickColumnCount, brickRowsCount, xStart, yStart, xEnd, yEnd, codeEmptyBrick, codeFinishBrick,
        codeFindPathBricks
    ) {
        const response = new Maze();
        const direction = {
            up: 'u',
            right: 'r',
            down: 'd',
            left: 'l'
        };
        let colQueue = [xStart],
            rowQueue = [yStart],
            pathFound = false,
            colLoc,
            rowLoc;

        while (colQueue.length > 0 && !pathFound) {
            colLoc = colQueue.shift();
            rowLoc = rowQueue.shift();

            if (colLoc > xStart) {
                if (map[colLoc - 1][rowLoc].state === codeFinishBrick) {
                    pathFound = true;
                }
            }
            if (colLoc < brickColumnCount - 1) {
                if (map[colLoc + 1][rowLoc].state === codeFinishBrick) {
                    pathFound = true;
                }
            }

            if (rowLoc > yStart) {
                if (map[colLoc][rowLoc - 1].state === codeFinishBrick) {
                    pathFound = true;
                }
            }
            if (rowLoc < brickRowsCount - 1) {
                if (map[colLoc][rowLoc + 1].state === codeFinishBrick) {
                    pathFound = true;
                }
            }

            if (colLoc > xStart) {
                if (map[colLoc - 1][rowLoc].state === codeEmptyBrick) {
                    colQueue.push(colLoc - 1);
                    rowQueue.push(rowLoc);
                    map[colLoc - 1][rowLoc].state = map[colLoc][rowLoc].state + direction.left;
                }
            }
            if (colLoc < xEnd) {
                if (map[colLoc + 1][rowLoc].state === codeEmptyBrick) {
                    colQueue.push(colLoc + 1);
                    rowQueue.push(rowLoc);
                    map[colLoc + 1][rowLoc].state = map[colLoc][rowLoc].state + direction.right;
                }
            }

            if (rowLoc > yStart) {
                if (map[colLoc][rowLoc - 1].state === codeEmptyBrick) {
                    colQueue.push(colLoc);
                    rowQueue.push(rowLoc - 1);
                    map[colLoc][rowLoc - 1].state = map[colLoc][rowLoc].state + direction.up;
                }
            }
            if (rowLoc < yEnd) {
                if (map[colLoc][rowLoc + 1].state === codeEmptyBrick) {
                    colQueue.push(colLoc);
                    rowQueue.push(rowLoc + 1);
                    map[colLoc][rowLoc + 1].state = map[colLoc][rowLoc].state + direction.down;
                }
            }
        }

        if (!pathFound) {
            response.success = false;
        } else {
            response.success = true;

            let path = map[colLoc][rowLoc].state,
                pathLength = path.length,
                currCol = xStart,
                currRow = yStart;

            for (let i = 0; i < pathLength - 1; i++) {
                if (path.charAt(i - 1) === direction.up) {
                    currRow -= 1;
                }
                if (path.charAt(i + 1) === direction.down) {
                    currRow += 1;
                }
                if (path.charAt(i + 1) === direction.right) {
                    currCol += 1;
                }
                if (path.charAt(i - 1) === direction.left) {
                    currCol -= 1;
                }

                map[currCol][currRow].state = codeFindPathBricks;
            }
        }

        response.payload = {
            map: map
        };

        return response;
    }
}

export default Maze;
