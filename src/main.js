import App from './App.vue'
import Vue from 'vue'
// eslint-disable-next-line sort-imports
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false;

new Vue({
    render: h => h(App)
}).$mount('#app');
