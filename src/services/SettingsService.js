class Settings {
    static status = {
        start: {
            code: 's',
            color: '#00FF00'
        },
        finish: {
            code: 'f',
            color: '#000'
        },
        empty: {
            code: 'e',
            color: '#F0F0F0',
        },
        fillBrick: {
            code: 'w',
            color: '#0000FF'
        },
        routeFindToSolve: {
            code: 'x',
            color: '#FFFF00'
        },
        default: '#FF0000'
    }
}

export default Settings;
