/* eslint-disable */
import Maze from '@/views/Maze.vue'
import {shallowMount} from '@vue/test-utils';

describe('Maze.vue', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallowMount(Maze, {});
    });
    it('renders without errors', () => {
        expect(wrapper.isVueInstance()).toBeTruthy();
    });

    let mockData = {
        canvas: {
            output: '',
            outputBg: null,
            width: 1200,
            height: 600,
            brickRowsCount: 10,
            brickColumnCount: 14,
            brickW: 50,
            brickH: 50,
            map: [],
            boundX: 0,
            boundY: 0,
            pathFound: false,
            mazeDifficulty: 3,
            status: {
                start: {
                    coordinates: {
                        x: 0,
                        y: 0
                    },
                    code: 's',
                    color: '#00FF00'
                },
                finish: {
                    coordinates: {
                        x: null,
                        y: null
                    },
                    code: 'f',
                    color: '#000'
                },
                empty: {
                    code: 'e',
                    color: '#F0F0F0',
                },
                fillTile: {
                    code: 'w',
                    color: '#0000FF'
                },
                routeFindToSolve: {
                    code: 'x',
                    color: '#FFFF00'
                },
                default: '#FF0000'
            },
            direction: {
                up: 'u',
                right: 'r',
                down: 'd',
                left: 'l'
            }
        }
    };

    it('can update data from vue component and expected to have a maze consisting of 15 columns and 10 rows', () => {
        wrapper.vm.$data.canvas.brickColumnCount = mockData.canvas.brickColumnCount;
        wrapper.vm.$data.canvas.brickRowsCount = mockData.canvas.brickRowsCount;

        // check if canvas data is an object
        expect(typeof wrapper.vm.$data.canvas).toBe('object');

        expect(wrapper.vm.$data.canvas.brickColumnCount).toBe(14);
        expect(wrapper.vm.$data.canvas.brickRowsCount).toBe(10);
    });

})
