/* eslint-disable */
import MazeServices from '../../../src/services/MazeService';

describe('maze', () => {

    it('create maze with 4 rows and 4 cols. Fill each brick with 30 width and 30 height and added 3 extra width and height for margins', () => {
        const Maze = new MazeServices();
        expect(Maze.constructor.create(
            [], 4, 4,30 ,30 ,0, 0, 3, 3,
            'e', 'w', 's', 'f', 99999
        )).toStrictEqual(
            [
                [
                    {"state": "s", "x": 0, "y": 0},
                    {"state": "e", "x": 0, "y": 33},
                    {"state": "e", "x": 0, "y": 66},
                    {"state": "e", "x": 0, "y": 99}
                    ],
                [
                    {"state": "e", "x": 33, "y": 0},
                    {"state": "e", "x": 33, "y": 33},
                    {"state": "e", "x": 33, "y": 66},
                    {"state": "e", "x": 33, "y": 99}
                    ],
                [
                    {"state": "e", "x": 66, "y": 0},
                    {"state": "e", "x": 66, "y": 33},
                    {"state": "e", "x": 66, "y": 66},
                    {"state": "e", "x": 66, "y": 99}
                    ],
                [
                    {"state": "e", "x": 99, "y": 0},
                    {"state": "e", "x": 99, "y": 33},
                    {"state": "e", "x": 99, "y": 66},
                    {"state": "f", "x": 99, "y": 99}
                    ]
            ]
        );
    });

    it('solved maze and received true success response + correct map for maze solving', () => {
        const Maze = new MazeServices();
        let map = [
            [
                {"state": "s", "x": 0, "y": 0},
                {"state": "e", "x": 0, "y": 33},
                {"state": "e", "x": 0, "y": 66},
                {"state": "e", "x": 0, "y": 99}
            ],
            [
                {"state": "e", "x": 33, "y": 0},
                {"state": "e", "x": 33, "y": 33},
                {"state": "e", "x": 33, "y": 66},
                {"state": "e", "x": 33, "y": 99}
            ],
            [
                {"state": "e", "x": 66, "y": 0},
                {"state": "e", "x": 66, "y": 33},
                {"state": "e", "x": 66, "y": 66},
                {"state": "e", "x": 66, "y": 99}
            ],
            [
                {"state": "e", "x": 99, "y": 0},
                {"state": "e", "x": 99, "y": 33},
                {"state": "e", "x": 99, "y": 66},
                {"state": "f", "x": 99, "y": 99}
            ]
        ]
        expect(Maze.constructor.solve(map, 4, 4, 0,
            0, 3, 3, 'e', 'f', 'x'
        ).success).toBe(true);

        expect(Maze.constructor.solve(map, 4, 4, 0,
            0, 3, 3, 'e', 'f', 'x'
        ).payload.map).toStrictEqual(
    [
                [
                    {
                        "state": "s",
                        "x": 0,
                        "y": 0
                    },
                    {
                        "state": "sd",
                        "x": 0,
                        "y": 33
                    },
                    {
                        "state": "sdd",
                        "x": 0,
                        "y": 66
                    },
                    {
                        "state": "sddd",
                        "x": 0,
                        "y": 99
                    }
                ],
                [
                    {
                        "state": "x",
                        "x": 33,
                        "y": 0
                    },
                    {
                        "state": "srd",
                        "x": 33,
                        "y": 33
                    },
                    {
                        "state": "srdd",
                        "x": 33,
                        "y": 66
                    },
                    {
                        "state": "srddd",
                        "x": 33,
                        "y": 99
                    }
                ],
                [
                    {
                        "state": "x",
                        "x": 66,
                        "y": 0
                    },
                    {
                        "state": "srrd",
                        "x": 66,
                        "y": 33
                    },
                    {
                        "state": "srrdd",
                        "x": 66,
                        "y": 66
                    },
                    {
                        "state": "srrddd",
                        "x": 66,
                        "y": 99
                    }
                ],
                [
                    {
                        "state": "x",
                        "x": 99,
                        "y": 0
                    },
                    {
                        "state": "x",
                        "x": 99,
                        "y": 33
                    },
                    {
                        "state": "x",
                        "x": 99,
                        "y": 66
                    },
                    {
                        "state": "f",
                        "x": 99,
                        "y": 99
                    }
                ]
            ]
        );
    });
})
